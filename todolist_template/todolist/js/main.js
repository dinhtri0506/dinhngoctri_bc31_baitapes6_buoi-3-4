import { RENDER_CONTROLLER } from "./controllers/render_controller.js";
import { TASK_CONTROLLERS } from "./controllers/task_controllers.js";
import { TASK } from "./models/task_models.js";
import { TASK_SERVICES } from "./services/task_services.js";

export let taskList = [];

document.getElementById("addItem").addEventListener("click", () => {
  TASK_SERVICES.add_data_to_API();
  TASK_CONTROLLERS.add_task(taskList);
  RENDER_CONTROLLER.render_task(taskList);
});

let getData = () => {
  TASK_SERVICES.get_data_from_API()
    .then((success) => {
      taskList = success.data;
      RENDER_CONTROLLER.render_task(taskList);
    })
    .catch((error) => {
      console.log(error);
    });
};
getData();

let removeTask = (id) => {
  let index = TASK_CONTROLLERS.find_index(taskList, id);
  TASK_SERVICES.remove_data(id);
  TASK_CONTROLLERS.remove_task(index);
  RENDER_CONTROLLER.render_task(taskList);
};
window.removeTask = removeTask;

let completeTask = (id) => {
  let index = TASK_CONTROLLERS.find_index(taskList, id);
  let taskContent = document.getElementById(`task${id}`).textContent;
  taskList[index].complete = TASK_CONTROLLERS.complete_task(index);
  let dataUpdated = new TASK(taskContent, taskList[index].complete);
  TASK_SERVICES.update_data(id, dataUpdated);

  let aToZList = TASK_CONTROLLERS.create_a_to_z_list();
  let zToAList = TASK_CONTROLLERS.create_a_to_z_list().reverse();
  RENDER_CONTROLLER.render_list(aToZList, zToAList, taskList);
};
window.completeTask = completeTask;

let sortTaskContentAZ = () => {
  RENDER_CONTROLLER.list_redered = "A-Z";

  RENDER_CONTROLLER.render_task(TASK_CONTROLLERS.create_a_to_z_list());
};
document.getElementById("two").addEventListener("click", sortTaskContentAZ);

let sortTaskContentZA = () => {
  RENDER_CONTROLLER.list_redered = "Z-A";
  RENDER_CONTROLLER.render_task(
    TASK_CONTROLLERS.create_a_to_z_list().reverse()
  );
};
document.getElementById("three").addEventListener("click", sortTaskContentZA);

let sortTaskByTime = () => {
  RENDER_CONTROLLER.list_redered = "default";
  RENDER_CONTROLLER.render_task(taskList);
};
document.getElementById("one").addEventListener("click", sortTaskByTime);
document.getElementById("all").addEventListener("click", sortTaskByTime);
