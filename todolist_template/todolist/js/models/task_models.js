export class TASK {
  constructor(_name, _complete) {
    this.name = _name;
    this.complete = _complete;
  }
}
