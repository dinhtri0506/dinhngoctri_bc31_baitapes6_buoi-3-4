export let RENDER_CONTROLLER = {
  render_task: (array) => {
    let toDoList = "";
    let completedList = "";
    array.map((item) => {
      let contentRendered = `
  <li>
      <span id="task${item.id}">${item.name}</span>
      <span class="buttons">
        <button class="complete" complete="${item.complete}" onclick="completeTask(${item.id})">
          <i class="fa fa-check"></i>
        </button>
        <button class="remove" onclick="removeTask(${item.id})">
          <i class="fa fa-minus"></i>
        </button>
      </span>
  </li>`;
      if (item.complete == false) {
        toDoList += contentRendered;
      } else {
        completedList += contentRendered;
      }
      return toDoList, completedList;
    });
    document.getElementById("todo").innerHTML = toDoList;
    document.getElementById("completed").innerHTML = completedList;
  },
  list_redered: "default",
  render_list: function (aToZ, zToA, defaultList) {
    if (this.list_redered == "A-Z") {
      this.render_task(aToZ);
    } else if (this.list_redered == "Z-A") {
      this.render_task(zToA);
    } else {
      this.render_task(defaultList);
    }
  },
};
