import { taskList } from "../main.js";
import { TASK } from "../models/task_models.js";

export let TASK_CONTROLLERS = {
  find_index: (array, id) => {
    let index = array.findIndex((item) => {
      return item.id == id;
    });
    return index;
  },
  add_task: (array) => {
    let taskInput = document.getElementById("newTask").value;
    let task = new TASK(taskInput, false);
    let _id = 1;
    if (array.length != 0) {
      _id = array[array.length - 1].id * 1 + 1;
    } else {
      _id = 1;
    }
    let clientSideTask = { ...task, id: _id };
    array.push(clientSideTask);
  },
  remove_task: (index) => {
    taskList.splice(index, 1);
  },
  complete_task: (index) => {
    let complete = taskList[index].complete;
    if (complete == false) {
      return true;
    } else {
      return false;
    }
  },
  compare_a_to_z: (a, b) => {
    let taskA = a.name.toUpperCase();
    let taskB = b.name.toUpperCase();
    if (taskA < taskB) {
      return -1;
    } else {
      return 1;
    }
  },
  create_a_to_z_list: () => {
    let newTaskList = [...taskList];
    let aToZList = newTaskList.sort(TASK_CONTROLLERS.compare_a_to_z);
    return aToZList;
  },
};
