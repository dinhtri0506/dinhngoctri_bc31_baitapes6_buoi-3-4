import { TASK } from "../models/task_models.js";

const BASE_URL = "https://62b07878e460b79df0469b79.mockapi.io/to-do-list";

export let TASK_SERVICES = {
  add_data_to_API: () => {
    let taskInput = document.getElementById("newTask").value;
    axios({
      url: BASE_URL,
      method: "POST",
      data: new TASK(taskInput, false),
    }).catch((error) => {
      console.log(error);
    });
  },
  get_data_from_API: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },
  remove_data: (id) => {
    axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    }).catch((error) => {
      console.log(error);
    });
  },
  update_data: (id, data) => {
    axios({
      url: `${BASE_URL}/${id}`,
      method: "PUT",
      data: data,
    }).catch((error) => {
      console.log(error);
    });
  },
};
